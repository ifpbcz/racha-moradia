firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      console.log( user );
    } else {
      alert("Você não está autenticado, precisa fazer o login para entrar!");
        window.location.href = "../html/login.html";
        
    }
  });
  
  function sair(){
    alert("Você está saindo da sua conta!");
  
    firebase.auth().signOut()
  .then(function() {
    console.log('Logout');
    
  }, function(error) {
    console.error( error );
  });

  }