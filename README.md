# Racha Moradia

[Clique aqui para acessar a pagina.](https://androidonthefly.github.io/Racha_Moradia/)

## Empresa: **FIS.LL**

> Projeto que facilita ao estudante encontrar outras pessoas para dividir despesas de moradia.

### Membros da Equipe de Desenvolvimento:

  - S�rgio Coelho de Carvalho              **Mat:** 201812010039

  - Fernando Silva de Oliveira             **Mat:** 201822010023

  - Francisco Iarlyson Santana de Andrade  **Mat:** 201822010021

  - Leandro Felix Lacerda                  **Mat:** 201822010026

  - Lucas Matheus Pereira De Lacerda       **Mat:** 201812010021

